<!-- SIDEBAR -->
                <aside class="sidebar">
                    <ul class="nav nav-stacked">
                        <li class="active" ><a href="/"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a></li>
                        <li  ><a href="log.php"><i class="fa fa-terminal fa-fw"></i>Logs</a></li>
                        <li  ><a href="help.php"><i class="fa fa-info fa-fw"></i>Help</a></li>
                        <li  ><a href="update.php"><i class="fa fa-download fa-fw"></i>Update firmware</a></li>
                    </ul>
                </aside>
                <!-- END: SIDEBAR -->